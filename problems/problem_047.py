# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lowercase_letter = False
    uppercase_letter = False
    digit = False
    has_special_char = False
    correct_length = False
    length = len(password)
    special_char = ["$", "!", "@"]
    if (length >= 6 and length <= 12):
        correct_length = True
    for l in password:
        if l.isalpha():
            if l.isupper():
                uppercase_letter = True
            elif l.islower():
                lowercase_letter = True
        elif l.isdigit():
            digit = True
        elif l in special_char:
            has_special_char = True
    return (lowercase_letter and uppercase_letter and digit and has_special_char and correct_length)
print(check_password('password'))
