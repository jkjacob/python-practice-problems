# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return
    max_value = values[0]
    for n in values:
        if n > max_value:
            max_value = n
    return max_value
