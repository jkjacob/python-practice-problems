# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(list):
    if len(list) % 2 == 1:
        B = list[0:(len(list)//2) + 1]
        C = list[(len(list)//2) + 1:]
    else:
        B = list[0:len(list)//2]
        C = list[(len(list)//2):]
    return (B,C)

print(halve_the_list([1, 2, 3, 4, 5, 6, 7]))
